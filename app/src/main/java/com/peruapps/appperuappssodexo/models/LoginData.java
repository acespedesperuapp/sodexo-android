package com.peruapps.appperuappssodexo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/

public class LoginData {
    @SerializedName("posts")
    @Expose
    private LoginItem item;

    public LoginItem getItem() {
        return item;
    }

    public void setItem(LoginItem item) {
        this.item = item;
    }
}