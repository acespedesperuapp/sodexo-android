package com.peruapps.appperuappssodexo.viewinterfaces;

public interface LoginView {

    void showValidationError();
    void loginSuccess();
    void loginError();
}
