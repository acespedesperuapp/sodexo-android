package com.peruapps.appperuappssodexo.data;

/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/

public class Conexion {
    public static final String URL_WEB_SERVICES ="http://pappstest.com:8000/api";
}
