package com.peruapps.appperuappssodexo.presenters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.peruapps.appperuappssodexo.data.Conexion;
import com.peruapps.appperuappssodexo.viewinterfaces.LoginView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/

public class LoginPresenter {

    private final String TAG = getClass().getSimpleName();

    private LoginView loginView;
    private Context context;

    public LoginPresenter(LoginView loginView, Context context) {

        this.loginView = loginView;
        this.context = context;
    }

    public void login(final String phoneNumber, final String password) {

        if (TextUtils.isEmpty(phoneNumber) && TextUtils.isEmpty(password)) {
            loginView.showValidationError();
        } else {

            RequestQueue queue = Volley.newRequestQueue(context);

            final String url = Conexion.URL_WEB_SERVICES + "/collaborators/login?include=user";
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {

                        JSONObject objResultado = new JSONObject(response);

                        if(response.equalsIgnoreCase("200")){
                            Log.v("SODEXO_LOG","Un error en el estado");
                        }else{
                            loginView.loginSuccess();

                    }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "Failure : " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    loginView.loginError();
                }
            }){
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params= new HashMap<>();
                    params.put("username", phoneNumber);
                    params.put("password", password);
                    return params;
                }
            };
            queue.add(stringRequest);

        }
    }
}