package com.peruapps.appperuappssodexo.view.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.peruapps.appperuappssodexo.R;

/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/

public class EditarInfoPersonalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_info_personal);
    }
}
