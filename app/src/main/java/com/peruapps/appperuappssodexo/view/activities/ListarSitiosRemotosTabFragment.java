package com.peruapps.appperuappssodexo.view.activities;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.peruapps.appperuappssodexo.R;

/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/
public class ListarSitiosRemotosTabFragment extends Fragment {


    public ListarSitiosRemotosTabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_listar_sitios_remotos_tab, container, false);
    }

}
