package com.peruapps.appperuappssodexo.view.activities;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.peruapps.appperuappssodexo.R;

/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/
public class ListarOperacionesFragment extends Fragment {
    public static ListarOperacionesFragment newInstance() {
        ListarOperacionesFragment fragment = new ListarOperacionesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_listar_operaciones, container, false);
    }
}