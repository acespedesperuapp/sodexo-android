package com.peruapps.appperuappssodexo.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.peruapps.appperuappssodexo.presenters.LoginPresenter;
import com.peruapps.appperuappssodexo.R;
import com.peruapps.appperuappssodexo.viewinterfaces.LoginView;

/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/
public class LoginActivity extends AppCompatActivity implements LoginView {

    private LoginPresenter presenter;
    EditText username, password;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.etUser);
        password = (EditText) findViewById(R.id.etPass);
        submit= (Button) findViewById(R.id.btnIngresar);

        presenter = new LoginPresenter(this, getBaseContext());

        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                presenter.login(
                        username.getText().toString().trim(),
                        password.getText().toString().trim()
                );
            }
        });

    }

    @Override
    public void showValidationError() {
        Toast.makeText(this, "Por favor introduzca una credencial y contraseña correcta", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginSuccess() {

        Intent intent = new Intent(LoginActivity.this, EditarInfoPersonalActivity.class);
        startActivity(intent);
    }

    @Override
    public void loginError() {
        Toast.makeText(this, "Credenciales de accesos invalidos!", Toast.LENGTH_SHORT).show();
    }
}
