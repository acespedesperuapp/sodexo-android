package com.peruapps.appperuappssodexo.view.activities;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.peruapps.appperuappssodexo.R;

/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/
public class ListarMenuOportunidadesSodexoFragment extends Fragment {
    private TabLayout tabLayoutOportunidades;
    private ViewPager viewPagerOportunidades;
    private int int_items = 2 ;
    public static ListarMenuOportunidadesSodexoFragment newInstance() {
        ListarMenuOportunidadesSodexoFragment fragment = new ListarMenuOportunidadesSodexoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_listar_menu_oportunidades_sodexo, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayoutOportunidades = (TabLayout) view.findViewById(R.id.tabOportunidades);
        viewPagerOportunidades = (ViewPager) view.findViewById(R.id.pageOportunidades);


        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.pageOportunidades);
        mViewPager.setAdapter(new ListarMenuOportunidadesSodexoFragment.MyAdapter(getChildFragmentManager()));
        // viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));

        tabLayoutOportunidades.post(new Runnable() {
            @Override
            public void run() {
                tabLayoutOportunidades.setupWithViewPager(viewPagerOportunidades);
            }
        });
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.bottom_nav_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    private class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 : return new ListarSitiosRemotosTabFragment();
                case 1 : return new ListarCorporacionesTabFragment();

            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return  Html.fromHtml("Sitios remotos") ;
                case 1 :
                    return  "Corporativos " ;
            }
            return null;
        }
    }
}
