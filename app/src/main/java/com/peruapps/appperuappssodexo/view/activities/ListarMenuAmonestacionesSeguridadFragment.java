package com.peruapps.appperuappssodexo.view.activities;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.peruapps.appperuappssodexo.R;

/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/
public class ListarMenuAmonestacionesSeguridadFragment extends Fragment {
    private TabLayout tabLayoutAmonestaciones;
    private ViewPager viewPagerAmonestaciones;
    private int int_items = 2 ;
    public static ListarMenuAmonestacionesSeguridadFragment newInstance() {
        ListarMenuAmonestacionesSeguridadFragment fragment = new ListarMenuAmonestacionesSeguridadFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_listar_menu_amonestaciones_seguridad, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayoutAmonestaciones = (TabLayout) view.findViewById(R.id.tabAmonestaciones);
        viewPagerAmonestaciones = (ViewPager) view.findViewById(R.id.pageAmonestaciones);


        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.pageAmonestaciones);
        mViewPager.setAdapter(new ListarMenuAmonestacionesSeguridadFragment.MyAdapter(getChildFragmentManager()));
        // viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));

        tabLayoutAmonestaciones.post(new Runnable() {
            @Override
            public void run() {
                tabLayoutAmonestaciones.setupWithViewPager(viewPagerAmonestaciones);
            }
        });
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.bottom_nav_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    private class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 : return new ListarAmonestacionesTabFragment();
                case 1 : return new ListarSeguridadCalidadTabFragment();

            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return  Html.fromHtml("Amonestaciones administrativas") ;
                case 1 :
                    return  "Seguridad \ny calidad " ;
            }
            return null;
        }
    }
}
