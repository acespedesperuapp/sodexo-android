package com.peruapps.appperuappssodexo.view.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.peruapps.appperuappssodexo.R;
/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/
public class MenuPrincipalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.navigation_home:
                                selectedFragment = ListarMenuCarrerasFragment.newInstance();
                                break;
                          case R.id.navigation_dashboard:
                                selectedFragment = ListarMenuAmonestacionesSeguridadFragment.newInstance();
                                break;
                             case R.id.navigation_notifications:
                                selectedFragment = ListarMenuOportunidadesSodexoFragment.newInstance();
                                break;
                            case R.id.navigation_dashboard2:
                                selectedFragment = ListarBeneficiosFragment.newInstance();
                                break;
                            case R.id.navigation_notifications3:
                                selectedFragment = ListarOperacionesFragment.newInstance();
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, ListarMenuCarrerasFragment.newInstance());
        transaction.commit();


    }
}
