package com.peruapps.appperuappssodexo.view.activities;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.peruapps.appperuappssodexo.R;

/**
 * Created by Alex Cespedes on 26/06/2019.
 * acespedesperuapps@gmail.com
 * <p>
 * Peru Apps
 * Trujillo, Peru.
 **/
public class ListarSeguridadCalidadTabFragment extends Fragment {

    String[] mobileArray = {"Android","IPhone","WindowsMobile","Blackberry",
            "WebOS","Ubuntu","Windows7","Max OS X"};

    public ListarSeguridadCalidadTabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_listar_seguridad_calidad_tab, container, false);
    }

}
